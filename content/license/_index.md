---
title: "License"
date: 2020-05-23T18:59:28+02:00
draft: false
---

This page illustrates the license and agreement on this blog site. The primary goal of this blog is to share and document knowledge in the form of blogsposts. The site is built with [Hugo](https://gohugo.io) using the [memE](https://themes.gohugo.io/theme/hugo-theme-meme/) theme. 

The memE template follows the [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.en) License. 

The intellectual property remeains to each and every author both in content posts as well as in references on this site. Everyone is free to use, share, refer to posts on this site and is encouraged in doing so. Everything is presented "AS IS", without warranty of any kind following a loosely defined "best effort" rule.  

For questions, remark or feedback please reach out on [Twitter](https://twitter.com/Ciberth).