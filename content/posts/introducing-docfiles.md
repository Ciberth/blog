---
title: "Introducing Docfiles"
date: 2020-05-23T21:08:39+02:00
draft: false
categories: ["test", "oops"]
tags: ["test", "ops"]
---

# Docfiles - The dotfiles for documentation

A brand new idea. Create a Git repository to store all handy commands you ever used.

Under development, more coming soon!